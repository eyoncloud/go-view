import { NumberConfig } from './Number/index'
import { TimeCommonConfig } from './TimeCommon/index'

export default [TimeCommonConfig, NumberConfig]
